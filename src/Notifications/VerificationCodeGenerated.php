<?php

namespace Doctoreto\Messenger\Notifications;

use Doctoreto\Messenger\Channels\VerificationSmsChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Doctoreto\Messenger\Messages\VerificationSmsMessage;

class VerificationCodeGenerated extends Notification
{
    use Queueable;

    protected $code;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(string $code)
    {
        $this->code = $code;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [VerificationSmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return (new VerificationSmsMessage())
                ->token($this->code)
                ->template('newVerificationCode');
    }
}
