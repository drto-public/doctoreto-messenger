<?php

namespace Doctoreto\Messenger\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Doctoreto\Messenger\Channels\SmsChannel;
use Doctoreto\Messenger\Messages\SmsMessage;

class DoctorAppointmentReport extends Notification implements ShouldQueue
{
    use Queueable;

    protected $doctorName;
    protected $appointmentCount;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        string $doctorName,
        string $appointmentCount
    ) {
        $this->doctorName = $doctorName;
        $this->appointmentCount = $appointmentCount;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return (new SmsMessage())
            ->header("با سلام حضور دکتر " . "$this->doctorName")
            ->content(
                "با افتخار به عرض می رسانیم که در هفته گذشته " .
                $this->appointmentCount . "نفر " .
                "از طریق وبسایت دکترتو از شما نوبت گرفته اند."
            )
            ->footer("با سپاس از همکاری شما");
    }
}
